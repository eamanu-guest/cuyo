#!/usr/bin/perl

#
#   Copyright 2008,2011 by Immanuel Halupczok
#   Maintenance modifications 2011 by the cuyo developers
#
#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 2 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#


use strict;

my $state = "start";
my $key;
my $processed;
my $depth = 0;
my $filename = "";
my $line_number = 0;
my $extra_comment;

print '# Message catalog for the game Cuyo.
# This is a partly generated file. The copyright of the original messages is identical to the one of the source files they are generated from. The copyright of the translations is by the translators.
# This file is distributed under the same license as the Cuyo package.
msgid ""
msgstr ""
"Project-Id-Version: Cuyo '.$ENV{VERSION}.'\n"
"Report-Msgid-Bugs-To: cuyo-devel@nongnu.org\n"
"POT-Creation-Date: 2009-02-19 15:20+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=ISO-8859-1\n"
"Content-Transfer-Encoding: 8bit\n"';

sub doError($) {
  my $e = shift;
  print STDERR "Line $.: $e\n";
  exit(1);
}

sub checkTranslate() {
  $depth == 1 or return 0;
  if ($key eq "name") { $extra_comment = "/* TRANSLATORS: level name */"; return 1; }
  if ($key eq "description") { $extra_comment = "/* TRANSLATORS: level description */"; return 1; }
  if ($key eq "author") { $extra_comment = "/* TRANSLATORS: level author */"; return 1; }
  return 0;
}

sub maybeTranslate($) {
  my $value = shift;
  if (checkTranslate()) {
    print '#: ' .$filename.':'.$line_number."\n";
    print 'msgid '.$value."\n";
    print 'msgstr ""'."\n\n";
  }
}



my %finiteStateMachine = (
  "start" => sub {
      if (s/^(<<)//) {
        $state = "in <<>>";
      } elsif (s/^([A-Za-z0-9_]+)//) {
        $key = $1;
        $processed .= $extra_comment if checkTranslate();
	$state = "[version]";
      } elsif (s/^(\})//) {
	$depth--;
	$state = "start";
      } else {
        doError("syntax error");
      }
    },
  "[version]" => sub {
      if (s/^(\[[A-Za-z0-9,_]*\])//) {
      }
      $state = "=";
    },
  "=" => sub {
      if (s/^(=)//) {
        $state = "value";
      } else {
        doError("'=' expected");
      }
    },
  "value" => sub {
      if (s/^([A-Za-z0-9_.-]+)//) {
	maybeTranslate('"'.$1.'"');
	$state = "after-value";
      } elsif (s/^("([^\\"]|\\.)*")//) {
	maybeTranslate($1);
	$state = "after-value";
      } elsif (s/^(\{)//) {
	$depth++;
	$state = "after-value";
      } elsif (s/^(<)//) {
	$state = "<>-value";
      } else {
        doError("syntax error");
      }
    },
  "<>-value" => sub {
      if (s/^([^>]+)//) {
      } elsif (s/^(>)//) {
	$state = "after-value";
      } else {
        doError("syntax error");
      }
    },
  "after-value" => sub {
      if (s/^(,|\*)//) {
        $state = "value";
      } else {
        $state = "start";
      }
    },
  "in <<>>" => sub {
       if (s/^([^>]+)//) {
	my $val = $1;
	pos($val) = 0;
        while ( $val =~ m/\G.*?message\s*\(\s*("[^"]*")\s*\)/gc ) {
		print '#: ' .$filename.':'.$line_number."\n";
		print 'msgid '.$1."\n";
		print 'msgstr ""'."\n\n";
	}
      } elsif (s/^(>>)//) {
	$state = "start";
      } elsif (s/^(>)//) {
      } elsif (s/^("([^\\"]|\\.)*")//) {
      } else {
        doError("syntax error (probably concerning string)");
      }
    },
);



while (<>) {
  $filename = $ARGV;
  $line_number = $.;
  # $_ contains one line of the input file

  chomp;

  s/^\s*//;  #remove spaces at beginning of line
  s/\s*$//;  #remove spaces at end of line

  if (s/(^[^#]*)#(.*)$/$1/) {  # Beginning of comment
    s/\s*$//;
  }

  # $_ does not contain comments anymore

  while ($_ ne "") {
    &{$finiteStateMachine{$state}}();
    s/^(\s*)//;
  }
  close (ARGV) if (eof);
}

$state == "start" or doError("Syntax error");
$depth == 0 or doError("Number of '{' != number of '}'");

exit;


