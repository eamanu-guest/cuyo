cuyo (2.1.0-1) UNRELEASED; urgency=medium

  * New upstream release
  * New maintainer (Closes: #916692).
    - Add myself to Maintainer field on d/control file.
  * Modify d/control field:
    - Bump Standards-Version to 4.5.1.
    - Add debhelper-compat = 13 to Build-Depends field.
    - Delete d/compat file.
  * Update Vcs-* on d/control files:
    - I create the project on salsa.
  * wrap-and-sort.
  * Bump d/watch version.

 -- Emmanuel Arias <emamnuelarias30@gmail.com>  Thu, 14 Feb 2019 00:55:40 +0000

cuyo (2.0.0brl1-3) unstable; urgency=low

  * add portuguese translation (Closes: 757090)
  * install translation files (fixes packaging error and adds pt)

 -- Bernhard R. Link <brlink@debian.org>  Sat, 18 Oct 2014 09:06:55 +0200

cuyo (2.0.0brl1-2) unstable; urgency=low

  * new maintenance upload
  - switch to debhelper compatibility level 8
  - bump Standards-Version
  - remve no longer needed lintian override

 -- Bernhard R. Link <brlink@debian.org>  Tue, 10 Dec 2013 12:06:55 +0100

cuyo (2.0.0brl1-1) unstable; urgency=low

  * new forked (without povray images) upstream version

 -- Bernhard R. Link <brlink@debian.org>  Thu, 23 Feb 2012 19:26:22 +0100

cuyo (2.0.0~beta3.brl1-1) unstable; urgency=low

  * new forked upstream version (without povray images)

 -- Bernhard R. Link <brlink@debian.org>  Fri, 20 Jan 2012 10:47:56 +0100

cuyo (2.0.0~beta2.brl1-1) experimental; urgency=low

  * new forked upstream version (without povray images)
  * update debian/copyright

 -- Bernhard R. Link <brlink@debian.org>  Thu, 12 Jan 2012 18:58:00 +0100

cuyo (2.~-1.2.brl6-1) experimental; urgency=low

  * new forked upstream version (without povray images)
  * modernize packaging (dpkg-buildflags, debhelper compat 7)
  * install fixed cuyo.desktop (Closes: 488298)

 -- Bernhard R. Link <brlink@debian.org>  Wed, 12 Oct 2011 15:44:44 +0000

cuyo (2.~-1.2.brl1-1) unstable; urgency=low

  * new forked upstream version without povray images
  - remove unneeded dependency on SDLimage

 -- Bernhard R. Link <brlink@debian.org>  Sat, 24 Jul 2010 20:18:28 +0200

cuyo (2.~-1.1.brl3-1) unstable; urgency=low

  * forked uptream version without povray images
  * fix build-errors with gcc-4.4 (Closes: 504947)
  * switch to source format 3.0

 -- Bernhard R. Link <brlink@debian.org>  Mon, 08 Mar 2010 11:38:50 +0100

cuyo (2.~-1.1-1) unstable; urgency=low

  * new bugfix upstream version
  - now also builds with gcc-4.3 (Closes: 461955)
  * increase debhelper compatibility level to 5

 -- Bernhard R. Link <brlink@debian.org>  Thu, 14 Feb 2008 21:26:51 +0100

cuyo (2.~-1.0-1) unstable; urgency=low

  * new upstream release

 -- Bernhard R. Link <brlink@debian.org>  Sat, 22 Dec 2007 15:27:40 +0100

cuyo (2.~-1.0~beta1-1) experimental; urgency=low

  * new upstream prerelease
  * split architecture independent data into cuyo-data package
  * add Homepage: and Vcs-* headers
  * update debian/copyright

 -- Bernhard R. Link <brlink@debian.org>  Sat, 27 Oct 2007 14:34:22 +0200

cuyo (2.~-1.0~alpha1-1) experimental; urgency=low

  * new upstream prerelease
  - new graphic routines (Closes: 208688)
  - adds small mode (Closes: 148822)
  * updated build-dependencies as this now uses SDL instead of QT
  * tidied up debian/rules
  * bumped Standards-version
  * add watch file

 -- Bernhard R. Link <brlink@debian.org>  Sun, 31 Dec 2006 16:44:33 +0100

cuyo (1.8.6-1) unstable; urgency=low

  * new version (mostly new levels and code needed for that)
  * general clean-up:
    - update debian/copyrights
    - move icon to /usr/share/pixmaps
    - quote all values in debian/menu
    - bump standards version
  * add versioned build-depend to new qt to catch the right ABI.

 -- Bernhard R. Link <brlink@debian.org>  Mon, 29 Aug 2005 17:48:43 +0200

cuyo (1.8.5-1) unstable; urgency=low

  * new upstream version

 -- Bernhard R. Link <brlink@debian.org>  Mon,  9 Aug 2004 12:09:56 +0200

cuyo (1.8.4-0) unstable; urgency=low

  * new upstream version including new levels and 
    new qt-searching (Closes: #255613, #263418)
  * building against newer xlibs will make the 
    dependencies more current (Closes: #253333)

 -- Bernhard R. Link <brlink@debian.org>  Fri, 23 Jul 2004 16:07:10 +0200

cuyo (1.8.3-4) unstable; urgency=low

  * The "I hate bugs I cannot reproduce" release,
    now totally circumventing the buggy autodetecting...

 -- Bernhard R. Link <brlink@debian.org>  Thu,  9 Oct 2003 20:22:48 +0200

cuyo (1.8.3-3) unstable; urgency=medium

  * The "This code is buggier than I thought" workaround.

 -- Bernhard R. Link <brlink@debian.org>  Thu,  9 Oct 2003 10:42:48 +0200

cuyo (1.8.3-2) unstable; urgency=medium

  * workaround for unreliable header-searching.

 -- Bernhard R. Link <brlink@debian.org>  Wed,  8 Oct 2003 14:32:48 +0200

cuyo (1.8.3-1) unstable; urgency=medium

  * new upstream version fixing:
    + 64-bit problems (Closes: #214430)
    + -pedantic problems with libqt3 (Closes: #213856)
  * fixed description (Closes: #207220)

 -- Bernhard R. Link <brlink@debian.org>  Tue,  7 Oct 2003 20:02:48 +0200

cuyo (1.8.1-1) unstable; urgency=low

  * new upstream version
  * binary moved to /usr/games,
    data to /usr/share/games/cuyo

 -- Bernhard R. Link <brlink@debian.org>  Tue, 19 Aug 2003 17:26:27 +0200

cuyo (1.7.0-1) unstable; urgency=low

  * new upstream version

 -- Bernhard R. Link <brlink@debian.org>  Tue,  1 Apr 2003 18:33:39 +0200

cuyo (1.6.1-1) unstable; urgency=low

  * New upstream version incorporating the local changes and removing
    compiler warning (did anyone try to run it on s390 before?)

 -- Bernhard R. Link <brlink@debian.org>  Wed, 18 Dec 2002 14:45:35 +0100

cuyo (1.6.0-3) unstable; urgency=low

  * Removed catching of non-standard signal

 -- Bernhard R. Link <brlink@debian.org>  Tue, 17 Dec 2002 20:03:16 +0100

cuyo (1.6.0-2) unstable; urgency=low

  * Run autoconf/make over it, to replace some buggy parts.

 -- Bernhard R. Link <brlink@debian.org>  Tue, 17 Dec 2002 11:40:08 +0100

cuyo (1.6.0-1) unstable; urgency=low

  * New upstream release.

 -- Bernhard R. Link <brlink@debian.org>  Wed, 11 Dec 2002 16:04:23 +0100

cuyo (1.05-1) unstable; urgency=high

  * Bugfix for an older bug  (Closes: #143892)

 -- Bernhard Link <debian@pcpool11.mathematik.uni-freiburg.de>  Wed, 26 Apr 2002 14:57:15 +0200

cuyo (1.04-1) unstable; urgency=medium

  * new upstream version with many cleanups (Closes: #132348)
  * Added absolute path to icon in menu (Closes: #141624)

 -- Bernhard Link <debian@pcpool11.mathematik.uni-freiburg.de>  Thu, 11 Apr 2002 15:37:02 +0200

cuyo (1.03-2) unstable; urgency=low

  * Fixed typo in debian/control (Closes: #124527)

 -- Bernhard Link <brl@pcpool11.mathematik.uni-freiburg.de>  Thu, 27 Dec 2001 15:53:52 +0100

cuyo (1.03-1) unstable; urgency=low

  * New upstream version (Level 'Jahreszeiten'(engl. seasons)

 -- Bernhard Link <brl@pcpool11.mathematik.uni-freiburg.de>  Thu, 13 Dec 2001 12:31:07 +0100

cuyo (1.02-1) unstable; urgency=low

  * New upstream version( including AM_MAINTAINER_MODE)

 -- Bernhard Link <brl@pcpool11.mathematik.uni-freiburg.de>  Tue, 27 Nov 2001 17:23:31 +0100

cuyo (1.00-7) unstable; urgency=low

  * Removed temorary files
  * added AM_MAINTAINERMODE so that it won't mess with autotools while
    building.
  * Initial upload, i hope. (Closes: #120070)

 -- Bernhard Link <brl@pcpool11.mathematik.uni-freiburg.de>  Tue, 27 Nov 2001 12:17:45 +0100

cuyo (1.00-6) unstable; urgency=low

  * Repacked the new upstream 1.00
  * added icon for the menu.

 -- Bernhard Link <brl@pcpool11.mathematik.uni-freiburg.de>  Sat, 17 Nov 2001 18:20:21 +0200
